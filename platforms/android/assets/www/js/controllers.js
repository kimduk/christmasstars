'use strict';

/* Controllers */

var starcatControllers = angular.module('starcatControllers', []);

starcatControllers.controller('StarListCtrl', ['$scope', 'Star',
    function ($scope, Star) {
        $scope.stars = Star.query();
        $scope.orderProp = 'age';
        $scope.topMenu = false;
        $scope.leftMenu = false;
        $scope.exit = function($event) {
            navigator.app.exitApp();
        }
        $scope.touching = function($event) {
            console.log(1);
        }

    }]);

starcatControllers.controller('StarDetailCtrl', ['$scope', '$routeParams', 'Star',
    function ($scope, $routeParams, Star) {
        $scope.star = Star.get({starId: $routeParams.starId}, function (star) {
            $scope.mainImageUrl = star.images[0];
        });

        $scope.setImage = function (imageUrl) {
            $scope.mainImageUrl = imageUrl;
        }
    }]);
