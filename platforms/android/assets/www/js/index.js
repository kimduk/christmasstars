/* App Module */

var christmasStarsApp = angular.module('christmasStarsApp', [
    'fsCordova',
//    'angular-gestures',
    'ngRoute',
    'ngTouch',
    'starcatAnimations',
    'starcatControllers',
    'starcatFilters',
    'starcatServices'
]);

christmasStarsApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/stars', {
                templateUrl: 'partials/star-list.html',
                controller: 'StarListCtrl'
            }).
            when('/stars/:starId', {
                templateUrl: 'partials/star-detail.html',
                controller: 'StarDetailCtrl'
            }).
            when('/about', {
                templateUrl: 'partials/about.html'
            }).
            when('/other-products', {
                templateUrl: 'partials/other-products.html'
            }).
            otherwise({
                redirectTo: '/stars'
            });
    }
]);

angular.module('fsCordova', [])
    .service('CordovaService', ['$document', '$q',
        function ($document, $q) {
            var d = $q.defer(),
                resolved = false;

            this.ready = d.promise;
            document.addEventListener('deviceready', function () {
                resolved = true;
                d.resolve(window.cordova);
            });

            setTimeout(function() {
                if(!resolved) {
                    if(window.cordova) d.resolve(window.cordova);
                }
            }, 3000);

        }]);
