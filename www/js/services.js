'use strict';

/* Services */

var starcatServices = angular.module('starcatServices', ['ngResource']);

starcatServices.factory('Star', ['$resource',
    function ($resource) {
        return $resource('stars/:starId.json', {}, {
            query: {method: 'GET', params: {starId: 'stars'}, isArray: true}
        });
    }]);
